import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {map} from 'rxjs/operators';
import {Actions, ofType} from '@ngrx/effects';

import {HouseItem} from '../../shared/shared.interfaces';
import {
    HouseMapsReqAction,
    HouseMapsReqSuccessAction,
    HouseMapStoreActionTypes
} from '../../root-store/house-map-store/house-map-store.actions';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-house-maps-list',
    templateUrl: 'house-maps-list.component.html',
    styleUrls: ['house-maps-list.component.scss']
})

export class HouseMapsListComponent implements OnInit, OnDestroy {
    private subscriptions: Subscription[] = [];

    houseItemsList: HouseItem[];

    constructor(private readonly store: Store<any>, private actions$: Actions) {
    }

    ngOnInit(): void {

        this.subscriptions.push(this.actions$.pipe(
            ofType<HouseMapsReqSuccessAction>(HouseMapStoreActionTypes.HOUSE_MAPS_REQ_SUCCESS),
            map((action) => {
                this.houseItemsList = action.payload['data'];
            }))
            .subscribe());

        this.store.dispatch(new HouseMapsReqAction());
    }

    ngOnDestroy(): void {
        this.subscriptions.forEach((s) => s.unsubscribe());
    }
}
