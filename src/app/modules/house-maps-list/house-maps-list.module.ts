import {NgModule} from '@angular/core';
import {MatGridListModule} from '@angular/material';
import {CommonModule} from '@angular/common';

import {HouseMapsListComponent} from './house-maps-list.component';
import {HouseMapsListRoutingModule} from './house-maps-list-routing.module';
import {HouseMapModule} from './house-map/house-map.module';


@NgModule({
    imports: [
        MatGridListModule,
        CommonModule,
        HouseMapsListRoutingModule,
        HouseMapModule
    ],
    exports: [],
    declarations: [HouseMapsListComponent],
    providers: [],
})
export class HouseMapsListModule {
}
