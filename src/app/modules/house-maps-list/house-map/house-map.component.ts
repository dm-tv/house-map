import {Component, ComponentFactoryResolver, Input, OnInit, ViewChild} from '@angular/core';
import {Store} from '@ngrx/store';

import {HouseItem} from '../../../shared/shared.interfaces';
import {CustomViewDirective} from './custom-view.directive';
import {HouseStore} from '../../../root-store/root-store.constants';
import {TemplateStoreState} from '../../../root-store/template-store/template-store.state';
import {CustomTemplateComponent} from './custom-template.component';

@Component({
    selector: 'app-house-map',
    templateUrl: './house-map.component.html',
    styleUrls: ['./house-map.component.scss']
})

export class HouseMapComponent extends CustomTemplateComponent implements OnInit {
    @Input()
    houseItem: HouseItem;

    @ViewChild(CustomViewDirective) customView: CustomViewDirective;

    constructor(private readonly store: Store<any>,
                protected readonly componentFactoryResolver: ComponentFactoryResolver) {
        super(componentFactoryResolver);
    }

    ngOnInit() {
        this.store
            .select(HouseStore.template)
            .subscribe((templateStoreState: TemplateStoreState) => {
                if (templateStoreState.activeTemplate) {
                    const viewContainerRef = this.customView.viewContainerRef;
                    this.loadComponentByTemplate(templateStoreState.activeTemplate, viewContainerRef);
                }
            });
    }

}