import {Component, OnInit, ViewChild} from '@angular/core';

import {CustomViewDirective} from '../../custom-view.directive';

@Component({
    selector: 'app-address-component',
    templateUrl: './address.component.html',
    styleUrls: ['./address.component.scss']
})

export class AddressComponent implements OnInit {
    data: any;

    @ViewChild(CustomViewDirective)
    customView: CustomViewDirective;

    constructor() {
    }

    ngOnInit(): void {
    }
}
