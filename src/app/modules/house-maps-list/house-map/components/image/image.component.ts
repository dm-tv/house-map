import {Component, OnInit, ViewChild} from '@angular/core';

import {CustomViewDirective} from '../../custom-view.directive';

@Component({
    selector: 'app-image-component',
    templateUrl: './image.component.html',
    styleUrls: ['./image.component.scss']
})

export class ImageComponent implements OnInit {
    data: any;
    defaultImageUrl = '';

    @ViewChild(CustomViewDirective)
    customView: CustomViewDirective;

    constructor() {
    }

    ngOnInit(): void {
        if (this.data.images[0]) {
            this.defaultImageUrl = this.data.images[0];
        }
    }
}
