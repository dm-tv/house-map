import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
    selector: '[custom-view]'
})
export class CustomViewDirective {
    constructor(public viewContainerRef: ViewContainerRef) { }
}
