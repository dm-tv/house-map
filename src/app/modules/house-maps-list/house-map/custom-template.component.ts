import {Component, ComponentFactoryResolver, Input, ViewChild} from '@angular/core';

import {HouseItem, HouseMap, Template} from '../../../shared/shared.interfaces';
import {HouseComponentName} from '../../../shared/shared.constants';
import {AddressComponent} from './components/address/address.component';
import {AreaComponent} from './components/area/area.component';
import {ImageComponent} from './components/image/image.component';
import {PriceComponent} from './components/price/price.component';
import {CustomViewDirective} from './custom-view.directive';

@Component({
    template: ''
})
export class CustomTemplateComponent {
    @Input()
    houseItem: HouseItem;

    @ViewChild(CustomViewDirective) customView: CustomViewDirective;

    constructor(protected readonly componentFactoryResolver: ComponentFactoryResolver) {
    }

    protected loadComponentByTemplate(template: Template, viewContainerRef): void {
        let componentRef;
        viewContainerRef.clear();
        template.template.forEach((houseMap: HouseMap) => {
            let component;
            let data = {};

            switch (houseMap.component) {
                case HouseComponentName.ADDRESS: {
                    component = AddressComponent;
                    data = {fullAddress: this.houseItem.full_address};
                    break;
                }
                case HouseComponentName.AREA: {
                    component = AreaComponent;
                    data = {area: this.houseItem.area};
                    break;
                }
                case HouseComponentName.IMAGE: {
                    component = ImageComponent;
                    data = {images: this.houseItem.images};
                    break;
                }
                case HouseComponentName.PRICE: {
                    component = PriceComponent;
                    data = {price: this.houseItem.price};
                    break;
                }
                default: {
                    throw new Error('Invalid component type!');
                }
            }
            const componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);

            componentRef = viewContainerRef.createComponent(componentFactory);
            componentRef.instance.data = data;

            if (houseMap.children) {
                const childrenViewRef = componentRef.instance.customView.viewContainerRef;
                this.loadComponentByTemplate({id: null, template: houseMap.children}, childrenViewRef);
                data['children'] = houseMap.children;
            }
        });
    }
}
