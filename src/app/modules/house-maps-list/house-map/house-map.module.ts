import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {HouseMapComponent} from './house-map.component';
import {CustomViewDirective} from './custom-view.directive';
import {ImageComponent} from './components/image/image.component';
import {PriceComponent} from './components/price/price.component';
import {AreaComponent} from './components/area/area.component';
import {AddressComponent} from './components/address/address.component';

@NgModule({
    imports: [CommonModule],
    exports: [HouseMapComponent],
    declarations: [
        HouseMapComponent,
        CustomViewDirective,
        ImageComponent,
        PriceComponent,
        AreaComponent,
        AddressComponent
    ],
    entryComponents: [
        ImageComponent,
        PriceComponent,
        AreaComponent,
        AddressComponent
    ],
    providers: [],
})
export class HouseMapModule {
}
