import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {HouseMapsListComponent} from './house-maps-list.component';

const routes: Routes = [
    {
        path: '',
        component: HouseMapsListComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HouseMapsListRoutingModule { }
