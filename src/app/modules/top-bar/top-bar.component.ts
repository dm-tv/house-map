import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {Subscription} from 'rxjs';
import {MatSelectChange} from '@angular/material';
import {takeWhile} from 'rxjs/operators';

import {Template} from '../../shared/shared.interfaces';
import {
    ChangeTemplateStoreAction,
    TemplatesReqAction
} from '../../root-store/template-store/template-store.actions';
import {HouseStore} from '../../root-store/root-store.constants';
import {TemplateStoreState} from '../../root-store/template-store/template-store.state';


@Component({
    selector: 'app-top-bar',
    templateUrl: './top-bar.component.html',
    styleUrls: ['./top-bar.component.scss']
})

export class TopBarComponent implements OnInit, OnDestroy {
    templates: Template[] = [];
    activeValue: Template;

    private subscriptions: Subscription[] = [];

    constructor(private readonly store: Store<any>) {
    }

    ngOnInit(): void {
        this.store.dispatch(new TemplatesReqAction());

        this.subscriptions.push(this.store
            .select(HouseStore.template)
            .pipe(
                takeWhile(() => {
                   return this.templates.length === 0;
                }))
            .subscribe(
            (templateStoreState: TemplateStoreState) => {
                this.templates = templateStoreState.availableTemplates;
                if (this.templates.length !== 0) {
                    this.activeValue = this.templates[0];
                    this.store.dispatch(new ChangeTemplateStoreAction(this.activeValue));
                }
            }));
    }

    public ngOnDestroy(): void {
        this.subscriptions.forEach((s: Subscription) => s.unsubscribe());
    }

    onChangeTemplate(e: MatSelectChange): void {
        this.store.dispatch(new ChangeTemplateStoreAction(e.value));
    }
}
