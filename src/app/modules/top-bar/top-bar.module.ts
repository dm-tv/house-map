import {NgModule} from '@angular/core';
import {MatSelectModule, MatToolbarModule} from '@angular/material';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {TopBarComponent} from './top-bar.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        MatToolbarModule,
        MatSelectModule
    ],
    exports: [TopBarComponent],
    declarations: [TopBarComponent],
    providers: [],
})
export class TopBarModule {
}
