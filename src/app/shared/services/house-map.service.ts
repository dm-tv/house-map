import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

import {HouseMap, Template} from '../shared.interfaces';

@Injectable()
export class HouseMapService {
    private readonly BASE_URL = 'http://demo4452328.mockable.io';
    private readonly TEMPLATES_PATH = '/templates';
    private readonly HOUSE_MAPS_PATH = '/properties';

    constructor(private readonly http: HttpClient) {
    }

    getTemplates(): Observable<Template[]> {
        return this.http.get<Template[]>(`${this.BASE_URL}${this.TEMPLATES_PATH}`);
    }

    getHouseMaps(): Observable<HouseMap[]> {
        return this.http.get<HouseMap[]>(`${this.BASE_URL}${this.HOUSE_MAPS_PATH}`);
    }
}
