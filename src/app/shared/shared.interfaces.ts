import {HouseComponentName} from './shared.constants';

export interface Template {
    id: number;
    template: HouseMap[];
}

export interface HouseMap {
    component: HouseComponentName;
    field: string;
    children?: HouseMap[];
}

export interface HouseItem {
    id: number;
    full_address: string;
    description: string;
    images: string[];
    area: number;
    price: number;
}
