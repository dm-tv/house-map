export enum HouseComponentName {
    'IMAGE' = 'IMAGE',
    'ADDRESS' = 'ADDRESS',
    'PRICE' = 'PRICE',
    'AREA' = 'AREA'
}
