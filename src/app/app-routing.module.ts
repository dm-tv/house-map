import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {PageNotFoundComponent} from './shared/modules/page-not-found/page-not-found.component';

const routes: Routes = [
  {
    path: 'list',
    loadChildren: './modules/house-maps-list/house-maps-list.module#HouseMapsListModule',
  },
  { path: '',   redirectTo: 'list', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
