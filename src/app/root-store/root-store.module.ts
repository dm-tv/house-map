import {NgModule} from '@angular/core';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';

import {TemplateStoreModule} from './template-store/template-store.module';
import {HouseMapStoreModule} from './house-map-store/house-map-store.module';

@NgModule({
  imports: [
    TemplateStoreModule,
    HouseMapStoreModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot([])
  ],
  exports: [],
  declarations: [],
  providers: [],
})
export class RootStoreModule {
}
