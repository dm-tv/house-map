import {Action} from '@ngrx/store';

import {templateStoreInitialState, TemplateStoreState} from './template-store.state';
import {TemplateStoreActionTypes} from './template-store.actions';


export function templateStoreReducer(state = templateStoreInitialState, action: Action): TemplateStoreState {
    switch (action.type) {
        case TemplateStoreActionTypes.TEMPLATES_REQ_SUCCESS: {
            return {
                ...state,
                availableTemplates: action['payload']
            };
        }
        case TemplateStoreActionTypes.CHANGE_TEMPLATE: {
            return {
                ...state,
                activeTemplate: action['payload']
            };
        }
        default: {
            return state;
        }
    }
}
