import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {EffectsModule} from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';

import {TemplateStoreEffects} from './template-store.effects';
import {templateStoreReducer} from './template-store.reducer';
import {HouseStore} from '../root-store.constants';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(
        HouseStore.template,
        templateStoreReducer),
    EffectsModule.forFeature([
        TemplateStoreEffects
    ])
  ]
})
export class TemplateStoreModule {
}
