import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Observable, of} from 'rxjs';
import {Action} from '@ngrx/store';
import {catchError, map, mergeMap} from 'rxjs/operators';
import {MatSnackBar, SimpleSnackBar} from '@angular/material';

import {
    TemplatesReqAction,
    TemplatesReqErrorAction,
    TemplatesReqSuccessAction,
    TemplateStoreActionTypes
} from './template-store.actions';
import {HouseMapService} from '../../shared/services/house-map.service';
import {Template} from '../../shared/shared.interfaces';

@Injectable()
export class TemplateStoreEffects {

    constructor(private actions$: Actions,
                private houseMapService: HouseMapService,
                private snackBar: MatSnackBar) {
    }

    @Effect()
    templatesReqEffect$: Observable<Action> = this.actions$.pipe(
        ofType<TemplatesReqAction>(TemplateStoreActionTypes.TEMPLATES_REQ),
        mergeMap(() => {
            return this.houseMapService.getTemplates();
        }),
        map((data: Template[]) => {
            return new TemplatesReqSuccessAction(data);
        }),
        catchError(() => {
            return of(new TemplatesReqErrorAction());
        }));

    @Effect({dispatch: false})
    templatesReqErrorEffect$: Observable<void> = this.actions$.pipe(
        ofType<TemplatesReqErrorAction>(TemplateStoreActionTypes.TEMPLATES_REQ_ERROR),
        map(() => {
            this.snackBar.openFromComponent(
                SimpleSnackBar,
                {
                    duration: 3000, data: {
                        message: 'Try again later',
                        action: 'Error'
                    }
                });
        }));

}
