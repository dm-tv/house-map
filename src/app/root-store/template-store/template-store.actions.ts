import {Action} from '@ngrx/store';

import {Template} from '../../shared/shared.interfaces';

export enum TemplateStoreActionTypes {
    CHANGE_TEMPLATE = '[template] Change template',
    TEMPLATES_REQ = '[template] Template req',
    TEMPLATES_REQ_SUCCESS = '[template] Template req success',
    TEMPLATES_REQ_ERROR = '[template] Template req error'
}

export class TemplatesReqAction implements Action {
    readonly type = TemplateStoreActionTypes.TEMPLATES_REQ;

    constructor() {
    }
}

export class TemplatesReqSuccessAction implements Action {
    readonly type = TemplateStoreActionTypes.TEMPLATES_REQ_SUCCESS;
    readonly payload: Template[];

    constructor(data: Template[]) {
        this.payload = data;
    }
}

export class TemplatesReqErrorAction implements Action {
    readonly type = TemplateStoreActionTypes.TEMPLATES_REQ_ERROR;

    constructor() {
    }
}

export class ChangeTemplateStoreAction implements Action {
    readonly type = TemplateStoreActionTypes.CHANGE_TEMPLATE;
    readonly payload: Template;

    constructor(public template: Template) {
        this.payload = template;
    }
}


