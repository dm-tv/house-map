import {Template} from '../../shared/shared.interfaces';

export interface TemplateStoreState {
  availableTemplates: any[];
  activeTemplate: Template;
}

export const templateStoreInitialState: TemplateStoreState = {
  availableTemplates: [],
  activeTemplate: null
};
