import {Action} from '@ngrx/store';

import {HouseMap} from '../../shared/shared.interfaces';

export enum HouseMapStoreActionTypes {
    HOUSE_MAPS_REQ = '[house-map] House maps req',
    HOUSE_MAPS_REQ_SUCCESS = '[house-map] House maps req success',
    HOUSE_MAPS_REQ_ERROR = '[house-map] House maps req error'
}

export class HouseMapsReqAction implements Action {
    readonly type = HouseMapStoreActionTypes.HOUSE_MAPS_REQ;

    constructor() {
    }
}

export class HouseMapsReqSuccessAction implements Action {
    readonly type = HouseMapStoreActionTypes.HOUSE_MAPS_REQ_SUCCESS;
    readonly payload;

    constructor(data: HouseMap[]) {
        this.payload = data;
    }
}

export class HouseMapsReqErrorAction implements Action {
    readonly type = HouseMapStoreActionTypes.HOUSE_MAPS_REQ_ERROR;
    readonly payload;
}
