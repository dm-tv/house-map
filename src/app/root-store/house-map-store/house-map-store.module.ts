import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EffectsModule} from '@ngrx/effects';

import {HouseMapStoreEffects} from './house-map-store.effects';

@NgModule({
    imports: [
        CommonModule,
        EffectsModule.forFeature([
            HouseMapStoreEffects
        ])
    ]
})
export class HouseMapStoreModule {
}
