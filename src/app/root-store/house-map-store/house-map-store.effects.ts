import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {HouseMapService} from '../../shared/services/house-map.service';
import {MatSnackBar, SimpleSnackBar} from '@angular/material';
import {Observable, of} from 'rxjs';
import {Action} from '@ngrx/store';
import {catchError, map, mergeMap} from 'rxjs/operators';

import {HouseMap} from '../../shared/shared.interfaces';
import {
    HouseMapsReqAction,
    HouseMapsReqErrorAction,
    HouseMapsReqSuccessAction,
    HouseMapStoreActionTypes
} from './house-map-store.actions';

@Injectable()
export class HouseMapStoreEffects {

    constructor(private actions$: Actions,
                private houseMapService: HouseMapService,
                private snackBar: MatSnackBar) {
    }

    @Effect()
    houseMapsReqEffect$: Observable<Action> = this.actions$.pipe(
        ofType<HouseMapsReqAction>(HouseMapStoreActionTypes.HOUSE_MAPS_REQ),
        mergeMap(() => {
            return this.houseMapService.getHouseMaps();
        }),
        map((data: HouseMap[]) => {
            return new HouseMapsReqSuccessAction(data);
        }),
        catchError(() => {
            return of(new HouseMapsReqErrorAction());
        }));

    @Effect({dispatch: false})
    houseMapsReqErrorEffect$: Observable<void> = this.actions$.pipe(
        ofType<HouseMapsReqErrorAction>(HouseMapStoreActionTypes.HOUSE_MAPS_REQ_ERROR),
        map(() => {
            this.snackBar.openFromComponent(
                SimpleSnackBar,
                {
                    duration: 3000, data: {
                        message: 'Try again later',
                        action: 'Error'
                    }
                });
        }));

}