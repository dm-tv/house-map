import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {MatSnackBarModule} from '@angular/material';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {RootStoreModule} from './root-store/root-store.module';
import {PageNotFoundModule} from './shared/modules/page-not-found/page-not-found.module';
import {TopBarModule} from './modules/top-bar/top-bar.module';
import {HouseMapService} from './shared/services/house-map.service';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        RootStoreModule,
        StoreDevtoolsModule.instrument({
            maxAge: 25
        }),
        BrowserAnimationsModule,
        CommonModule,
        HttpClientModule,
        MatSnackBarModule,
        PageNotFoundModule,
        TopBarModule
    ],
    providers: [HouseMapService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
