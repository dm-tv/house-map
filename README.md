# HouseMap Description

The main task is to implement house map configurable component.
Need to create Angular component that displays house map. The map should display the
next data:
- image (images)
- address (full_address)
- price (price)
- area (area)

Order of displaying elements can be
configured using the different templates.

#####Requirements
- The application should work;
- Use JS(ES6), React (+ redux on your choice);
- There are no special requirements for application’s user interface (all illustrations
below are schematic), however, functional and easy-to-use interfaces will get higher
rates. Adaptability will also be a plus.
- Application should work in last two versions of desktop browsers (IE, Chrome, Safari,
Firefox).

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
